local Hanker = require 'hanker'

local test = {}

local currentMenu = "main"
local menus = {}
menus.main = {
	{
		name = "Play Game",
		action = function()
			print("played game")
		end,
	},
	{
		name = "Multiplayer",
		action = function()
			print("Multiplayer")
		end,
	},
	{
		name = "Options",
		action = function()
			currentMenu = "options"
		end,
	},
	{
		name = "Credits",
		action = function()
			print("credits")
		end,
	},
	{
		name = "Quit",
		action = function()
			love.event.push('quit')
		end,
	},
}
menus.options = {
	{
		name = "Option: do the thing",
		action = function()
			print("did the thing")
		end,
	},
	{
		name = "Back to Menu",
		action = function()
			currentMenu = "main"
		end,
	},
	{
		name = "Use grace period?",
		checkbox = true,
		action = function(checked)
			print(checked)
		end,
	},
	{
		name = "grace period (s)",
		textbox = true,
		action = function(text)
			print(text)
		end,
	},
}

local title = Hanker.newLabel("Your HOT new game", 48)
title:setAlign('center', 'top')
title:setTextAlign('center')
title:setWidth(400)
title:useFloatingHeight()
title:top():setRelative(Hanker.TOP, 30)
title:centerX():setRelative(Hanker.CENTERX)

local function drawCheckbox(self, x, y, w, h)
	if self.state.checked then
		love.graphics.setColor(.4, .4, .4)
		love.graphics.rectangle('fill', x, y, w, h)
	end

	if (self.over or self.selected) and not self.pressed then
		-- This button is moused over
		love.graphics.setColor(1, 1, 1, 1)
		love.graphics.setLineWidth(2)
	else
		love.graphics.setColor(.7, .7, .7, 1)
	end
	love.graphics.rectangle('line', x, y, w, h)
	love.graphics.setLineWidth(1)
end

for _, actions in pairs(menus) do
	local lastControl = title
	local nextYOffset = 50
	actions.cursor = Hanker.newFixedGridCursor()
	for _, action in ipairs(actions) do
		if action.checkbox then
			action.checkbox = Hanker.newClickable(drawCheckbox)
			action.checkbox:setAlign('left')
			action.checkbox:setDimensions(40, 40)
			action.checkbox:top():setRelative(lastControl:bottom(), nextYOffset)
			action.checkbox:left():setRelative(lastControl:left())

			action.label = Hanker.newLabel(action.name, 28)
			action.label:setTextAlign('left')
			action.label:setWidth(250)
			action.label:useFloatingHeight()
			action.label:top():setRelative(action.checkbox:top())
			action.label:left():setRelative(action.checkbox:right(), 10)

			actions.cursor:addRow(action.checkbox)
			lastControl = action.checkbox
		elseif action.textbox then
			action.label = Hanker.newLabel(action.name, 28)
			action.label:setTextAlign('left')
			action.label:setWidth(200)
			action.label:useFloatingHeight()
			action.label:top():setRelative(lastControl:bottom(), nextYOffset)
			action.label:left():setRelative(lastControl:left())

			action.textbox = Hanker.newEditBox(28)
			action.textbox:setWidth(200)
			action.textbox:setHeightLines(1)
			action.textbox:top():setRelative(action.label:top())
			action.textbox:left():setRelative(action.label:right(), 10)

			actions.cursor:addRow(action.textbox)
			lastControl = action.label
		else
			action.button = Hanker.newButton(action.name, 28)
			action.button:setTextAlign('center')
			action.button:setAlign('center')
			action.button:setWidth(300)
			action.button:useFloatingHeight()
			action.button:top():setRelative(lastControl:bottom(), nextYOffset)
			action.button:centerX():setRelative(lastControl:centerX())
			actions.cursor:addRow(action.button)
			lastControl = action.button
		end

		nextYOffset = 10
	end
end

function test.update()
	local menu = menus[currentMenu]
	menu.cursor()

	if currentMenu == "main" then
		title()
	end
	for _, action in ipairs(menu) do
		if action.button then
			if action.button():isClicked() then
				action.action()
			end
		elseif action.textbox then
			action.label()
			if action.textbox():isClicked() then
				action.action(action.textbox:getEditText())
			end
		elseif action.checkbox then
			if action.checkbox():isClicked() then
				action.value = not action.value
				action.checkbox.state.checked = action.value
				action.action(action.value)
			end
			action.label()
		end
	end
end

return test
