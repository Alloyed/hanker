# Modules/Classes 

* \[class\] [Anchor](#anchor)
* \[class\] [Hanker.Context](#hankercontext)
* \[class\] [Control](#control)
* \[class\] [DrawMixin](#drawmixin)
* \[class\] [RectMixin](#rectmixin)
* \[class\] [TextObjectMixin](#textobjectmixin)
* \[class\] [ClickMixin](#clickmixin)
* \[class\] [ScrollMixin](#scrollmixin)
* \[class\] [Drawable](#drawable)
* \[class\] [Label](#label)
* \[class\] [Clickable](#clickable)
* \[class\] [Button](#button)
* \[class\] [ScrollArea](#scrollarea)
* \[class\] [ScrollList](#scrolllist)
* \[class\] [EditBox](#editbox)
* \[module\] [Hanker](#hanker)
* \[class\] [Rect](#rect)
# AnchorAxis

*[enum]*

# Anchor

the building block of UI layout. Layout Rects are defined as a
collection of anchor points, which can anchored absolutely, to other
Rects, etc.

## Anchor:getValue()<a id="anchorgetvalue"></a>

Return this anchor's current value

**Return Value**: value in Anchor units

## Anchor:getViewportValue()<a id="anchorgetviewportvalue"></a>

Returns value normalized to the current viewport. Viewport values are
what you ultimately get when we actually draw or compare control sizes
to the  mouse.


**Return Value**: value in Viewport units.

## Anchor:getAxis(otherAnchor)<a id="anchorgetaxis"></a>

Set Anchor to use the same settings as the provided anchor.

**Parameters**:
- *otherAnchor*: another anchor on the same axis as this one.

**Return Value**: self

## Anchor:setAbsolute(value)<a id="anchorsetabsolute"></a>

Set Anchor to an absolute value.

**Parameters**:
- *value*: the value this anchor should return. In Anchor units.

**Return Value**: self

## Anchor:setRelative(parentAnchor, offset)<a id="anchorsetrelative"></a>

Set Anchor relative to another anchor plus or minus a fixed offset.
This means when the parent anchor changes, so will this one.

**Parameters**:
- *parentAnchor*: another anchor on the same axis.
- *offset*: a fixed offset from the parent anchor. In Anchor units.

**Return Value**: self

## Anchor:set(parentAnchor, offset)<a id="anchorset"></a>

alias for [Anchor:setRelative()](#anchorsetrelative)

**Parameters**:
- *parentAnchor*: another anchor on the same axis.
- *offset*: a fixed offset from the parent anchor. In Anchor units.

**Return Value**: self

## Anchor:setLerp(beginAnchor, endAnchor, value)<a id="anchorsetlerp"></a>

Set anchor between two different anchors along the same axis.
A value of 0 places this anchor at `beginAnchor`.
A value of 1 places this anchor at `endAnchor`.

**Parameters**:
- *beginAnchor*: the first parent anchor
- *endAnchor*: the second parent anchor
- *value*: the lerp fraction between the two anchors.

**Return Value**: self

## Hanker.Context

A hanker context represents the top level state of the UI. multiple
contexts can be used to keep UI state separate, or you can use the default
context by calling context methods directly on the `Hanker` object.

## Context:draw()<a id="contextdraw"></a>

draw the currently active UI controls.

## Context:mousePressed(x, y, m)<a id="contextmousepressed"></a>

call when a mouse button has been pressed.

**Parameters**:
- *x*: cursor x coordinate, in viewport units
- *y*: cursor y coordinate, in viewport units
- *m*: mouse button

**See also**: [love.mousepressed](#lovemousepressed)
## Context:mouseReleased(x, y, m)<a id="contextmousereleased"></a>

call when a mouse button has been released.

**Parameters**:
- *x*: cursor x coordinate, in viewport units
- *y*: cursor y coordinate, in viewport units
- *m*: mouse button

**See also**: [love.mousereleased](#lovemousereleased)
## Context:mouseMoved(x, y)<a id="contextmousemoved"></a>

call when the mouse cursor has been moved.

**Parameters**:
- *x*: cursor x coordinate, in viewport units
- *y*: cursor y coordinate, in viewport units

**See also**: [love.mousemoved](#lovemousemoved)
## Context:updateMouse(x, y)<a id="contextupdatemouse"></a>

Update the UI mouse position. In most cases you should rely on
mouseMoved to update the mouse for you instead.

**Parameters**:
- *x*: cursor x coordinate, in viewport units
- *y*: cursor y coordinate, in viewport units

## Context:wheelMoved(x, y)<a id="contextwheelmoved"></a>

call when the mouse wheel has been moved.

**Parameters**:
- *x*: wheel x coordinate, in mousewheel units
- *y*: wheel y coordinate, in mousewheel units

**See also**: [love.wheelmoved](#lovewheelmoved)
## Context:textInput(text)<a id="contexttextinput"></a>

call when the user has input some text.

**Parameters**:
- *text*: the text to be input. utf8 string.

**See also**: [love.textinput](#lovetextinput)
## Context:keyboardKeyPressed(key)<a id="contextkeyboardkeypressed"></a>

call when the user has pressed a key on their keyboard.

**Parameters**:
- *key*: the keycode of the pressed key. scancode unused.

**See also**: [love.keypressed](#lovekeypressed)
## Context:cursorKeyPressed(cursorKey)<a id="contextcursorkeypressed"></a>

call when the user starts performing an action that should be translated into
UI cursor movement. This is application-defined: you may want gamepads or
the arrow keys or even tab/shift-tab to cause cursor movement. You can
also programmatically change the cursor, although for many cases you can
just directly manipulate the current cursor yourself.

**Parameters**:
- *cursorKey*: the cursorKey that has been pressed.

**See also**: [Hanker.Cursors](#hankercursors)
## Context:cursorKeyPressed(cursorKey)<a id="contextcursorkeypressed"></a>

call when the user finishes performing an action that should be
translated into UI cursor movement. If there is no begin/end to the
action, you should call pressed and released at the same time.

**Parameters**:
- *cursorKey*: the cursorKey that has been released.

**See also**: [Hanker.Cursors](#hankercursors)
## Context:updateFrame(dt)<a id="contextupdateframe"></a>

Update the UI time. this is used to drive animations.

**Parameters**:
- *dt*: The amount of time to update the UI by, in seconds.

**See also**: [love.update](#loveupdate)
## Context:setDefaultFont(hankerFont)<a id="contextsetdefaultfont"></a>

Set the font controls should use when no other font is provided in
their constructor. controls will _not_ update their font if you change the
default after they have been constructed, they will just keep the old
default font.

**Parameters**:
- *hankerFont*: the Hanker.Font object

**Return Value**: self

## Context:getDefaultFont()<a id="contextgetdefaultfont"></a>

*[function]*

**Return Value**: hankerFont

## Context:setDefaultStyle(controlType, style)<a id="contextsetdefaultstyle"></a>

Set the default parent style for a given control type.
nil is valid and is treated as "no default style".

**Parameters**:
- *controlType*: the control type string.
- *style*: the control style table.

**Return Value**: self

## Context:getDefaultStyle(controlType)<a id="contextgetdefaultstyle"></a>

*[function]*

**Parameters**:
- *controlType*: the control type string.

**Return Value**: style

# Control

Controls are the elements you actually build UI with. Every frame you
intend to draw or update a control, you must should call it, and then every
control called that frame that was not marked hidden will be drawn in the
order they were called.

**Usage**:
```lua
local myControl = Hanker.newLabel("My Control")
myControl:setDimensions(100, 20)
myControl:left():setAbsolute(100)
myControl:top():setAbsolute(100)
function love.update(dt)
  if showLabel then
    -- recomputes the internal text object if needed, and draws.
     myControl()
  else
     -- ony recompute the text object
     myControl('hide')
  end
end
```

# ControlHideBehavior

*[enum]*

# Control(shouldHide)<a id="control"></a>

Computes the control, and adds it to the DrawList. What this means
varies per `controlType`, but this can be seen as "activating" the control to be used in this screen.

**Parameters**:
- *shouldHide*: when set to 'hide', will not add this control to the DrawList. this can be useful when a control needs to be computed but not drawn.

**Usage**:
```lua
local myControl1 = Hanker.newLabel("My Control 1")
local myControl2 = Hanker.newLabel("My Control 2")
local myControl3 = Hanker.newLabel("My Control 3")
function love.update()
  myControl1() -- will show "My Control 1" on screen.
  myControl2('hide') -- will compute the size of myControl, so it can be anchored to, but it won't be visible
  -- myControl3 will not be recomputed or shown this frame, unless it is called.
end
```

# DrawMixin

defines draw behavior.

**See also**: [Drawable](#drawable)
## DrawMixin:setDrawFn(drawFn)<a id="drawmixinsetdrawfn"></a>

Specifies a custom draw function. The needs of draw functions for any
given control are pretty varied, so for any control you'd like to override
the draw function for, A good start would be to copy-paste the existing
defaultDraw() function for that control type and modify it from there.

**Parameters**:
- *drawFn*

**Return Value**: self

## DrawMixin:defaultDraw(x, y, w, h)<a id="drawmixindefaultdraw"></a>

All controls specify a defaultDraw function for diagnostic and example
purposes. For your own applications, custom draw implementations can provide
more complex and visually appealing results.

**Parameters**:
- *x*
- *y*
- *w*
- *h*

# RectMixin

controls with a RectMixin have a rectangle bounding box and
participate in the anchor layout system. As a general rule, if you can
call a method with a Rect, you can call that same method on a RectMixin
control, and use them interchangably.

**See also**: [Rect](#rect)
## RectMixin:setMask(mask)<a id="rectmixinsetmask"></a>

masked controls will only draw within the confines of the given mask.
This is implemented internally using `love.graphics.intersectScissor()`.

**Parameters**:
- *mask*: The mask Rect or RectMixin object. Set to nil to unset mask.

## RectMixin:isInside(x, y)<a id="rectmixinisinside"></a>

returns true when the given position is inside the control's rectangle.

**Parameters**:
- *x*: in viewport units
- *y*: in viewport units

# TextObjectMixin

TextObjectMixin controls can display text.
By default, objects with a textObjectMixin will default to having a floating
size. this means they will automatically resize to fit the text provided.
You can also have them automatically resize to fit a specific number of
lines, or have their width be fixed and grow downwards to compensate. Line
wrapping is automatically supported, just specify a fixed width.

**See also**: [Label](#label)
## TextObjectMixin:setText(text)<a id="textobjectmixinsettext"></a>

*[function]*

**Parameters**:
- *text*

**Return Value**: self

## TextObjectMixin:getText()<a id="textobjectmixingettext"></a>

*[function]*

**Return Value**: text

## TextObjectMixin:setTextSize(textSize)<a id="textobjectmixinsettextsize"></a>

*[function]*

**Parameters**:
- *textSize*

**Return Value**: self

## TextObjectMixin:setTextAlign(textAlignX, textAlignY)<a id="textobjectmixinsettextalign"></a>

Sets the text alignment within the control's bounding rectangle. This
is independent of the rectangle itself, so for example, you could have a
rectangle that's right-aligned to the screen, and have the text inside it be
left or center aligned to that rectangle.

**Parameters**:
- *textAlignX*: (default 'left')
- *textAlignY*: (default 'top')

**Return Value**: self

# TextAlignX

*[enum]*

# TextAlignY

*[enum]*

## TextObjectMixin:setFont(font)<a id="textobjectmixinsetfont"></a>

*[function]*

**Parameters**:
- *font*: The `Hanker.Font` object.

**Return Value**: self

## TextObjectMixin:UseFloatingHeight()<a id="textobjectmixinusefloatingheight"></a>

when called, this control will automatically set its height to fit all
the text it needs to display. This means that text that goes beyond your
specified width will wrap and grow downwards instead of out. You can undo
this by using `control:setHeight()`.

**Return Value**: self

## TextObjectMixin:UseFloatingSize()<a id="textobjectmixinusefloatingsize"></a>

when called, this control will automatically set its size to fit all
the text it needs to display. You can undo this by using
`control:setHeight()` or `control:setWidth()`

**Return Value**: self

## TextObjectMixin:setHeightLines(numLines)<a id="textobjectmixinsetheightlines"></a>

Automatically sets the height of the control to fit `numLines` worth
of text.

**Parameters**:
- *numLines*

**Return Value**: self

## TextObjectMixin:drawText(x, y, w, h)<a id="textobjectmixindrawtext"></a>

Draw current text within the provided bounding box

**Parameters**:
- *x*
- *y*
- *w*
- *h*

**See also**: [DrawMixin:setDrawFn](#drawmixinsetdrawfn)
# ClickMixin

ClickMixin controls can be clicked and selected. They provide the
"button" behavior in the Button control.

## ClickMixin:isPressed()<a id="clickmixinispressed"></a>

Returns true when this control is being currently pressed down.

**Return Value**: isPressed

## ClickMixin:setClicked(clicked)<a id="clickmixinsetclicked"></a>

Sets this current control's state to clicked. this can be used to emulate mouseclicks.

**Parameters**:
- *clicked*

## ClickMixin:isClicked()<a id="clickmixinisclicked"></a>

A control will be clicked when a player has decided to interact with
this object. When using a mouse, this means a player has pressed and
released mouse1 over this control, but it can come from other sources too.

**Return Value**: clicked

## ClickMixin:isDoubleClicked()<a id="clickmixinisdoubleclicked"></a>

A control will be double-clicked when the player has clicked within
500 ms of a previous single click. A double click will also register as a single click.

**Return Value**: clicked

## ClickMixin:isSelected()<a id="clickmixinisselected"></a>

A control can be selected, which really should be "focused". it
means that a Cursor has it as the active element.

**See also**: [Cursor](#cursor)
## ClickMixin:isOver()<a id="clickmixinisover"></a>

Is true if a mouse is currently over this control.

**Return Value**: isOver

# ScrollMixin

ScrollMixin controls can be scrolled along one dimension.

## ScrollMixin:getScrollOffset()<a id="scrollmixingetscrolloffset"></a>

Get the scroll area offset along the scroll axis. This will be 0 at the top/left of the page and grow as you scroll down/right.

**Return Value**: scrollOffset in Anchor units

## ScrollMixin:getViewScrollSize()<a id="scrollmixingetviewscrollsize"></a>

Returns the size of the scroll list control along the scroll axis.

**Return Value**: viewScrollSize in Anchor units.

## ScrollMixin:getTotalScrollSize()<a id="scrollmixingettotalscrollsize"></a>

Returns the size of the entire list including offscreen entries.

**Return Value**: totalScrollSize in Anchor units.

## ScrollMixin:getMaxScrollOffset()<a id="scrollmixingetmaxscrolloffset"></a>

Returns the scroll offset needed to line up the last entry with the
bottom of the scroll list. When `shouldClampMax` is set, this will be the
max possible offset.

**Return Value**: maxScrollOffset in Anchor units.

## ScrollMixin:setShouldClamp(clampMin, clampMax)<a id="scrollmixinsetshouldclamp"></a>

Sets clamp points for the control.

**Parameters**:
- *clampMin*: when true, list offset will never go below 0.
- *clampMax*: when true, list offset will never go above `getMaxScrollOffset()`.

**Return Value**: self

**See also**: [ScrollList:scroll](#scrolllistscroll), [ScrollList:getMaxScrollOffset](#scrolllistgetmaxscrolloffset)
## ScrollMixin:scroll(offset, instantly)<a id="scrollmixinscroll"></a>

scrolls this control to the given offset.

**Parameters**:
- *offset*: the target offset along the scroll axis. Measured in Anchor units.
- *instantly*: When set, immediately jump to the given offset. Otherwise, animate smoothly toward it.

**Return Value**: self

# Drawable

Provides an empty control that you can specify a custom draw function
for. This can be used for anything, including images/videos or other visual
elements

**Usage**:
```lua
local image = love.graphics.newImage("image.png")
local myDrawable = Hanker.newDrawable(function(x, y, w, h)
    local sx = image:getWidth() / w
    local sy = image:getHeight() / h
    love.graphics.draw(image, x, y, 0, sx, sy)
end)
myDrawable:setDimensions(image:getDimensions())
```

**See also**: [Control](#control), [RectMixin](#rectmixin), [DrawMixin](#drawmixin), [DrawMixin:setDrawFn](#drawmixinsetdrawfn)
## Context:newDrawable(drawFn)<a id="contextnewdrawable"></a>

*[function]*

**Parameters**:
- *drawFn*: (default nil) The custom drawfn. if no draw fn is provided, a colored square will be drawn

**Return Value**: drawable

# Label

Labels layout text within the UI.

**Usage**:
```lua
local myLabel = Hanker.newLabel("my funky label", 20, Hanker.getDefaultFont())
```

**See also**: [Control](#control), [RectMixin](#rectmixin), [DrawMixin](#drawmixin), [TextObjectMixin](#textobjectmixin)
## Context:newLabel(text, textSize, font)<a id="contextnewlabel"></a>

*[function]*

**Parameters**:
- *text*: the button text.
- *textSize*: the initial text size. defaults to the current love.graphics font size
- *font*: (default nil) the hanker font to use. If not specified, we will use `Context:getDefaultFont()`

**Return Value**: label

# Clickable

Provides a button-like clickable control with no styling. You can
provide a custom drawFn to create your own visuals.

**See also**: [Control](#control), [RectMixin](#rectmixin), [DrawMixin](#drawmixin), [TextObjectMixin](#textobjectmixin), [DrawMixin:setDrawFn](#drawmixinsetdrawfn)
## Context:newClickable(drawFn)<a id="contextnewclickable"></a>

*[function]*

**Parameters**:
- *drawFn*: (default nil) The custom drawfn. if no draw fn is provided, a basic button will be drawn

**Return Value**: clickable

# Button

Buttons are a clickable thing with some text on it. If you would like
to have a button with no text, check out Clickable instead.

**See also**: [Control](#control), [RectMixin](#rectmixin), [TextObjectMixin](#textobjectmixin), [ClickMixin](#clickmixin)
## Context:newButton(text, textSize, font)<a id="contextnewbutton"></a>

*[function]*

**Parameters**:
- *text*: the button text.
- *textSize*: (default nil) the initial text size.
- *font*: (default nil) the hanker font to use.

**Return Value**: button

# ScrollArea

Scroll Areas are like scroll lists, except you can use it with
controls of arbitrary different sizes. It doesn't provide any optimization
for controls that aren't currently visible, so only use it if Scroll Lists
can't do the job, and if there won't be too many controls inside the scroll
area.

**See also**: [Control](#control), [RectMixin](#rectmixin), [ScrollMixin](#scrollmixin)
## Context:newScrollArea(axis)<a id="contextnewscrollarea"></a>

*[function]*

**Parameters**:
- *axis*: (default 'y') the axis to scroll along, either 'x' or 'y'. 2d scrolls are (deliberately) not supported.

# ScrollList

ScrollLists are lists of equally sized controls that can be scrolled
alongside one axis. Scroll lists keep a list of abstract entries, and will
only associate visible entries with a control, So scroll lists can have very
large numbers of entries without issue.

**See also**: [Control](#control), [RectMixin](#rectmixin), [ScrollMixin](#scrollmixin)
## Context:newScrollList(cellScrollSize, initFn, axis, entries)<a id="contextnewscrolllist"></a>

*[function]*

**Parameters**:
- *cellScrollSize*: the size of each control along the scrollable axis
- *initFn*: the constructor function for the control. the control may be a reused control from a pool, but it's your responsibility to properly clear any lingering state. params `(control, entry, entryIndex) -> ()`
- *axis*: the axis to scroll along, 'x' or 'y'. (default 'y')
- *entries*: the initial entry list. the ScrollList control will take ownership of this entry list, and you can get it back to modify using `ScrollList:updateEntries()`. (default empty list)

**Return Value**: scrollList

## ScrollList:getEntries()<a id="scrolllistgetentries"></a>

Returns the list of entries. This should only be used in a read-only
context: editing the entry list directly is not a safe operation.
Entries can be literally anything, but they will sometimes be used as keys
and checked for equality, so keep that in mind.

**Return Value**: entries

## ScrollList:updateEntries(updateFn)<a id="scrolllistupdateentries"></a>

Change/modify the list of entries.

**Parameters**:
- *updateFn*: An updater function. You should edit the entry list from in here, and _not_ retain a reference to the entry list. params `(entryList) -> ()`.

**Return Value**: self

## ScrollList:getNumCellsPerPage()<a id="scrolllistgetnumcellsperpage"></a>

Gets the max number of control cells that can be visible at any given time.

**Return Value**: numCellsPerPage

## ScrollList:getNumCellsTotal()<a id="scrolllistgetnumcellstotal"></a>

Gets the total number of cells in the list. This is the same as the number of list entries.

**Return Value**: numCellsTotal

## ScrollList:setActiveCursor(activeCursor)<a id="scrolllistsetactivecursor"></a>

Mark the scroll list as being controlled by a ScrollListCursor.

**Parameters**:
- *activeCursor*: the active ScrollListCursor.

**Return Value**: self

**See also**: [ScrollListCursor](#scrolllistcursor)
## ScrollList:iterateActiveControls()<a id="scrolllistiterateactivecontrols"></a>

returns an iterator for each entry control in view. Use this to write per-control logic.

**Return Value**: iterator returning `(entryIndex, entryControl)`

**Usage**:
```lua
function updateListUI(list)
   list()
   for entryIndex, entryControl in pairs(list:iterateActiveControls) do
       if entryControl:isClicked() then ... end -- your logic goes here
   end
end
```

## ScrollList:SetControlScrollSize(controlScrollSize)<a id="scrolllistsetcontrolscrollsize"></a>

Set the size of each control along the scroll axis. Every control must have the same size.

**Parameters**:
- *controlScrollSize*: in Anchor units

**Return Value**: self

## ScrollList:setControlPadding(controlPaddingX, controlPaddingY)<a id="scrolllistsetcontrolpadding"></a>

Set the amount of empty space between each entry control.
Along the scroll axis, this is only between controls.
Against the scroll axis, this applied between each list edge.

**Parameters**:
- *controlPaddingX*: in Anchor units
- *controlPaddingY*: in Anchor units

**Return Value**: self

## ScrollList:getCellScrollSize()<a id="scrolllistgetcellscrollsize"></a>

returns the total size of the cell along the scroll axis, including padding.

**Return Value**: cellScrollSize in Anchor units.

## ScrollList:focus()<a id="scrolllistfocus"></a>

Returns the point that should be considered the "focal point" of the
list. The default value of this is at the center of the list.

**Return Value**: focus the focus anchor along the scroll axis.

## ScrollList:scrollToIndex(entryIndex, instantly)<a id="scrolllistscrolltoindex"></a>

scroll the list such that the cell at the given index is centered at
the scroll focus.

**Parameters**:
- *entryIndex*: the entry index
- *instantly*: when set, immediately jump to index.

**Return Value**: self

## ScrollList:scrollIndexIntoView(entryIndex, instantly)<a id="scrolllistscrollindexintoview"></a>

If the given entry index is out of the "safe" view area, scroll it
into view. If it's already in view, don't do anything.

**Parameters**:
- *entryIndex*: the entry index
- *instantly*: when set, immediately jump to index.

**Return Value**: self

# EditBox

Editboxes are clickable boxes that can be used to let players input
text. Editboxes are primarily driven by a keyboard, but gamepad/IME users
can programmatically fill in an editbox as if it were a normal label, too.

**See also**: [Control](#control), [RectMixin](#rectmixin), [DrawMixin](#drawmixin), [ClickMixin](#clickmixin), [TextObjectMixin](#textobjectmixin)
## Context:newEditBox(textSize, font, placeholderText)<a id="contextneweditbox"></a>

*[function]*

**Parameters**:
- *textSize*: (default nil) the initial text size.
- *font*: (default nil) the hanker font to use.
- *placeholderText*: (default "") specifies placeholder text.

**Return Value**: editBox

## EditBox:insertTextAtCursor(text)<a id="editboxinserttextatcursor"></a>

*[function]*

**Parameters**:
- *text*

## EditBox:setTextRange(rangeStartIndex., cursorIndex)<a id="editboxsettextrange"></a>

*[function]*

**Parameters**:
- *rangeStartIndex.*: If nil, then no range is specified. if defined. it's the range start.
- *cursorIndex*: interpreted as range end when a range start is defined. ranges can be backwards.

**Return Value**: self

## EditBox:setCursorIndex(cursorIndex)<a id="editboxsetcursorindex"></a>

*[function]*

**Parameters**:
- *cursorIndex*: The position within the text string the player cursor should be placed at.

**Return Value**: self

## EditBox:isTextRangeSelected()<a id="editboxistextrangeselected"></a>

*[function]*

**Return Value**: isSelected

## EditBox:moveCursorLeft(moveType, asRange)<a id="editboxmovecursorleft"></a>

*[function]*

**Parameters**:
- *moveType*
- *asRange*

## EditBox:moveCursorRight(moveType, asRange)<a id="editboxmovecursorright"></a>

*[function]*

**Parameters**:
- *moveType*
- *asRange*

## EditBox:moveCursorToMouse(startIndex)<a id="editboxmovecursortomouse"></a>

*[function]*

**Parameters**:
- *startIndex*: if specified, selects a range from the startIndex to the cursor.

**Return Value**: self

## EditBox:deleteText(moveType)<a id="editboxdeletetext"></a>

*[function]*

**Parameters**:
- *moveType*

## EditBox:setEditText(text)<a id="editboxsetedittext"></a>

replaces setText(). sets the current user-editable string within the edit box.

**Parameters**:
- *text*

**Return Value**: self

## EditBox:setEditText()<a id="editboxsetedittext"></a>

with edit boxes, use setEditText instead

## EditBox:getEditText()<a id="editboxgetedittext"></a>

replaces getText(). returns the current user-editable string within the edit box.

**Return Value**: editText

## EditBox:getEditText()<a id="editboxgetedittext"></a>

with edit boxes, use getEditText instead

## EditBox:setEditMargin(editMargin)<a id="editboxseteditmargin"></a>

*[function]*

**Parameters**:
- *editMargin*: the margin, in UI units, to place between the inner text and the control boundaries.

**Return Value**: self

## EditBox:getEditMargin()<a id="editboxgeteditmargin"></a>

*[function]*

**Return Value**: editMargin

## EditBox:setPlaceholderText(text)<a id="editboxsetplaceholdertext"></a>

Sets placeholder text. When a textbox is empty and deactivated, the
placeholder text will be visible instead to give users a clue to fill it in.

**Parameters**:
- *text*

**Return Value**: self

## EditBox:getPlaceholderText()<a id="editboxgetplaceholdertext"></a>

*[function]*

**Return Value**: text

## EditBox:setCharacterLimit(maxCodepoints)<a id="editboxsetcharacterlimit"></a>

*[function]*

**Parameters**:
- *maxCodepoints*: (nil for unlimited)

**Return Value**: self

## EditBox:getCharacterLimit()<a id="editboxgetcharacterlimit"></a>

*[function]*

**Return Value**: maxCodepoints (nil for unlimited)

## EditBox:setPasswordMode(isPasswordMode)<a id="editboxsetpasswordmode"></a>

sets password mode, which will replace all visible characters with asterisks.

**Parameters**:
- *isPasswordMode*

**Return Value**: self

## EditBox:isPasswordMode()<a id="editboxispasswordmode"></a>

*[function]*

**Return Value**: isPasswordMode

## Context:newFont(loveFont)<a id="contextnewfont"></a>

Creates a new font from an existing love2d font.

**Parameters**:
- *loveFont*: an existing ttf/image font

**Return Value**: HankerFont

## Context:newMSDFFont(loveFont, nativeSize, sharpness)<a id="contextnewmsdffont"></a>

Creates a new font from an existing MSDF image font.
MSDF fonts can be scaled up and down visually with fewer artifacts,
making it a good choice for text in 3D or heavily animated environments.

**Parameters**:
- *loveFont*: an existing image font
- *nativeSize*: the size the font is authored to take up
- *sharpness*

**Return Value**: HankerFont

# Hanker

The main entry point of Hanker. Most Context functions can be called
on Hanker directly like so:
```
Hanker.newContext():newButton()
-- Can be translated to
Hanker.newButton()
```

**See also**: [Context](#context)
## Hanker.newContext()<a id="hankernewcontext"></a>

*[function]*

**Return Value**: context

## Hanker.setViewport()<a id="hankersetviewport"></a>

*[function]*

**Return Value**: context

## Hanker.setUnscaledDimensions()<a id="hankersetunscaleddimensions"></a>

*[function]*

**Return Value**: context

# Rect

A rect represents a rectangle in 2d space.

## Rect:getAABB()<a id="rectgetaabb"></a>

returns the final calculated space this rect takes up.

**Return Value**: x, y, w, h in viewport units

## Rect:isInside(x, y)<a id="rectisinside"></a>

performs a point-AABB hit test, in viewport units.

**Parameters**:
- *x*
- *y*

**Return Value**: isInside

## Rect:getAxisSize(axis)<a id="rectgetaxissize"></a>

*[function]*

**Parameters**:
- *axis*: the axis to measure along.

**Return Value**: size in Anchor units

## Rect:getWidth()<a id="rectgetwidth"></a>

*[function]*

**Return Value**: width in Anchor units

## Rect:getHeight()<a id="rectgetheight"></a>

*[function]*

**Return Value**: height in Anchor units

## Rect:getDimensions()<a id="rectgetdimensions"></a>

*[function]*

**Return Value**: width, height in Anchor units

## Rect:setAxisSize(axis, size)<a id="rectsetaxissize"></a>

directly set the size of the rect along an axis. The size can also be
defined by setting both the begin/end anchors of a rect, in which case this
is ignored.

**Parameters**:
- *axis*: the axis to set
- *size*: in Anchor units

**Return Value**: self

## Rect:setWidth(width)<a id="rectsetwidth"></a>

*[function]*

**Parameters**:
- *width*: in Anchor units

**Return Value**: self

## Rect:setHeight(height)<a id="rectsetheight"></a>

*[function]*

**Parameters**:
- *height*: in Anchor units

**Return Value**: self

## Rect:setDimensions(width, height)<a id="rectsetdimensions"></a>

*[function]*

**Parameters**:
- *width*: in Anchor units
- *height*: in Anchor units

**Return Value**: self

# RectAlignX

*[enum]*

# RectAlignY

*[enum]*

## Rect:setAlign(xAlign, yAlign)<a id="rectsetalign"></a>

sets which anchors should user-defined, and which anchors should be
defined by the `size` variable. By default, the top and the left anchors are
the alignment anchors, but for example you could set the xAlign to 'center'
and then se can anchor this rect from its center, and have the left and
right anchors grow out automatically.

**Parameters**:
- *xAlign*: either 'top', 'center', or 'bottom'
- *yAlign*: either 'left', 'center', or 'right'

**Return Value**: self

## Rect:left()<a id="rectleft"></a>

*[function]*

**Return Value**: anchor

## Rect:right()<a id="rectright"></a>

*[function]*

**Return Value**: anchor

## Rect:centerX()<a id="rectcenterx"></a>

*[function]*

**Return Value**: anchor

## Rect:top()<a id="recttop"></a>

*[function]*

**Return Value**: anchor

## Rect:bottom()<a id="rectbottom"></a>

*[function]*

**Return Value**: anchor

## Rect:centerY()<a id="rectcentery"></a>

*[function]*

**Return Value**: anchor

## Rect:axisBegin(axis)<a id="rectaxisbegin"></a>

*[function]*

**Parameters**:
- *axis*

**Return Value**: anchor

## Rect:axisEnd(axis)<a id="rectaxisend"></a>

*[function]*

**Parameters**:
- *axis*

**Return Value**: anchor

## Rect:axisCenter(axis)<a id="rectaxiscenter"></a>

*[function]*

**Parameters**:
- *axis*

**Return Value**: anchor

