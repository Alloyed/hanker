local Hanker = require 'hanker'

local test = {}

local cursor = Hanker.newFixedGridCursor()

local skipmap = {
	{x = 3, y=2},
	{x = 4, y=1},
	{x = 4, y=2},
}
local function skip(x, y)
	for _, p in ipairs(skipmap) do
		if p.x == x and p.y == y then
			return true
		end
	end
	return false
end
local grid = {}
for x=1, 10 do
	for y=1, 3 do
		if not skip(x, y) then
			if x == 3 and y == 1 then
				local btn = Hanker.newButton("Large")
				btn:setWidth(60*2+2)
				btn:setHeight(60*2+2)
				btn:top():setAbsolute(y*62)
				btn:left():setAbsolute(x*62)

				table.insert(grid, btn)
				cursor:setGridControl(btn, x, y)
				cursor:setGridControl(btn, x, y+1)
				cursor:setGridControl(btn, x+1, y)
				cursor:setGridControl(btn, x+1, y+1)
			else
				local btn = Hanker.newButton(("%dx%d"):format(x, y))
				btn:setWidth(60)
				btn:setHeight(60)
				btn:top():setAbsolute(y*62)
				btn:left():setAbsolute(x*62)

				table.insert(grid, btn)
				cursor:addRow(btn, x)
			end
		end
	end
end

function test.update(dt)
	cursor()

	for _, btn in ipairs(grid) do
		btn()
	end
end

return test
