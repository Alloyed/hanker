const opt = {
	//charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
	fontSize: 24,
	textureWidth: 512,
	textureHeight: 512,
	distanceRange: 6,
	transparent: false
};

const generateBMFont = require('msdf-bmfont');
const fs = require('fs');
const path = require('path');
const Canvas = require('canvas');

var fontFile = process.argv[2];
var fontName = path.basename(fontFile).replace(/\.[^/.]+$/, "");

generateBMFont(fontFile, opt, (error, textures, font) => {
	if (error) throw error;
	textures.forEach((texture, index) => {
		var texFileName = fontName + "-" + index + ".png";
		font.pages.push(texFileName);
		//var invTexture = invertImage(texture);
		fs.writeFile(texFileName, texture, (err) => {
			if (err) throw err;
		});
	});

	writeBMFontFile(font);
});

function writeBMFontFile(font) {
	var stream = fs.createWriteStream(fontName + ".fnt");
	stream.once('open', function(fd) {
		// info
		stream.write("info padding=");
		var padding = font.info.padding;
		for(i = 0; i < padding.length; i++) {
			stream.write(padding[i].toString());
			if(i != padding.length - 1) {
				stream.write(",");
			}
		}
		// Libgdx does not read spacing info but does require a space after padding info so let's write it anyway
		stream.write(" spacing=");
		var spacing = font.info.spacing;
		for(i = 0; i < spacing.length; i++) {
			stream.write(spacing[i].toString());
			if(i != spacing.length - 1) {
				stream.write(",");
			}
		}
		stream.write("\n");
		// common
		stream.write("common lineHeight=" + roundAwayFromZero(font.common.lineHeight));
		stream.write(" base=" + roundAwayFromZero(font.common.base) + " pages=" + font.common.pages);
		stream.write("\n");
		// pages
		var pages = font.pages;
		for(i = 0; i < pages.length; i++) {
			stream.write("page id=" + i + " file=\"" + pages[i] + "\"\n");
		}
		// chars
		var chars = font.chars;
		stream.write("chars count=" + chars.length + "\n");

		//hardcoded space
		stream.write("char");
		stream.write(" id=" + 32);
		stream.write(" x=" + 0);
		stream.write(" y=" + 0);
		stream.write(" width=" + 0);
		stream.write(" height=" + 0);
		stream.write(" xoffset=" + 0);
		stream.write(" yoffset=" + 0);
		stream.write(" xadvance=" + 15);
		stream.write(" page=" + 0);
		stream.write("\n");

		for(i = 0; i < chars.length; i++) {
			stream.write("char");
			stream.write(" id=" + chars[i].id);
			stream.write(" x=" + chars[i].x);
			stream.write(" y=" + chars[i].y);
			stream.write(" width=" + chars[i].width);
			stream.write(" height=" + chars[i].height);
			stream.write(" xoffset=" + roundAwayFromZero(chars[i].xoffset));
			stream.write(" yoffset=" + roundAwayFromZero(chars[i].yoffset));
			stream.write(" xadvance=" + roundAwayFromZero(chars[i].xadvance));
			stream.write(" page=" + chars[i].page);
			stream.write("\n");
		}
		// kernings
		var kernings = font.kernings;
		stream.write("kernings count=" + kernings.length + "\n");
		for(i = 0; i < kernings.length; i++) {
			stream.write("kerning");
			stream.write(" first=" + kernings[i].first);
			stream.write(" second=" + kernings[i].second);
			stream.write(" amount=" + roundAwayFromZero(kernings[i].amount));
			stream.write("\n");
		}
		stream.end();
	});
}

function roundAwayFromZero(val) {
	return val;
	//return val > 0 ? Math.ceil(val) : Math.floor(val);
}

// Adapted from https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Pixel_manipulation_with_canvas#Grayscaling_and_inverting_colors
function invertImage(imgBuffer) {
	var Image = Canvas.Image;
	var canvas = new Canvas(opt.textureWidth, opt.textureHeight);
	var context = canvas.getContext('2d');

	var img = new Image;
	img.src = imgBuffer;
	context.drawImage(img, 0, 0);
	var imgData = context.getImageData(0, 0, canvas.width, canvas.height)
	var data = imgData.data;

	for (var i = 0; i < data.length; i += 4) {
		data[i]     = 255 - data[i];     // red
		data[i + 1] = 255 - data[i + 1]; // green
		data[i + 2] = 255 - data[i + 2]; // blue
	}

	context.putImageData(imgData, 0, 0);

	return canvas.toBuffer();
};
