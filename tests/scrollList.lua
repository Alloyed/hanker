-- simple scrollList test
local Hanker = require 'hanker'

local test = {}

local removeMe

local list
do
	local function init(entry, i)
		-- This function gets called to add a control to the control pool. you
		-- can do extra init in here if you want but keep in mind we might
		-- assign another entry to this button and if that happens we don't
		-- call this function. Do most/all of the heavy lifting inside of
		-- update() instead.
		return Hanker.newButton(entry)
	end

	local entries = {}
	for i=string.byte('a'), string.byte('z') do
		for j=string.byte('a'), string.byte('z') do
			for k=string.byte('a'), string.byte('z') do
				table.insert(entries, "label "
					.. string.char(i)
					.. string.char(j)
					.. string.char(k))
			end
		end
	end

	local buttonHeight = 20
	list = Hanker.newScrollList(buttonHeight, init, 'y', entries)
	list:setControlPadding(2, 4)
	list:setHeight(500)
	list:setWidth(100)
	list:left():setAbsolute(30)
	list:top():setAbsolute(30)
end

local scrollValue = Hanker.newLabel("scroll")
scrollValue:setWidth(200)
scrollValue:useFloatingHeight()
scrollValue:left():setRelative(list:right(), 10)
scrollValue:top():setRelative(list:top())

local cursor = Hanker.newScrollListCursor(list, {
	shouldWrapY = true,
	useFixedScrollPoint = false,
})

local function reverse(a, b) return a > b end
function test.update(dt)
	cursor()
	list()

	if cursor:isLeaving() then
		love.event.push('quit')
	end

	local toRemove = {}
	for entryIndex, button in list:iterateActiveControls() do
		if button:isOver() or button:isSelected() then
			button:setText(button.entry .. "!")
		else
			button:setText(button.entry)
		end

		if button:isClicked() then
			-- editing the entries table here would cause a desync between the
			-- control indices and the entry indices, so defer it until after.
			table.insert(toRemove, entryIndex)
		end
	end
	
	table.sort(toRemove, reverse)
	for _, i in ipairs(toRemove) do
		table.remove(list:getEntries(), i)
	end

	local offset = math.floor(.5+list:getScrollOffset())
	local numEntries = #list:getEntries()
	local text = ("offset:%d\nnumEntries:%d"):format(offset, numEntries)
	scrollValue():setText(text)
end

return test
