-- Example Usage: lua doc.lua source.lua ... sourceN.lua output.md

local tagTypes = {
    builtin = "@",
    customSection = "$",
    customField = "&",
}
local tagTypeClass = tagTypes.builtin .. tagTypes.customSection .. tagTypes.customField

local sectionTags = {
    ["module"] = true,
    ["class"] = true,
    ["function"] = true,
    ["table"] = true,
    ["enum"] = true,
}

local function splitName(name)
    local ret = {}
    for part in string.gmatch(name, "[^.:]+") do
        table.insert(ret, part)
    end
    return ret
end

local function addSection(doc, tag, name, isCustom)
    local section = {
        tag = tag,
        name = name,
        namePath = splitName(name),
        isCustom = isCustom,
    }
    table.insert(doc, section)
    return section
end

local function paramTable(str)
    local ident = str:match("(%S+)")
    if not ident then
        error(("Could not parse param/field tag '%s'"):format(str))
    end
    local _ident, desc = str:match("(%S+)%s+(.*)$")
    if _ident and desc then
        return {name = _ident, description = desc}
    else
        return {name = ident}
    end
end

local function addField(lineNumber, section, tag, content, isCustom)
    if section == nil then
        error(string.format("line %d: Field tag does not have a section", lineNumber))
    end

    if tag == "param" then
        if not section.params then section.params = {} end
        table.insert(section.params, paramTable(content))
    elseif tag == "field" then
        if not section.fields then section.fields = {} end
        table.insert(section.fields, paramTable(content))
    elseif tag == "value" then
        if not section.fields then section.fields = {} end
        table.insert(section.fields, paramTable(content))
    elseif tag == "return" then
        section.returnValue = content
    elseif tag == "desc" then
        if section.description then
            section.description = section.description .. "\n" .. content
        else
            section.description = content
        end
    elseif tag == "see" then
        if not section.see then section.see = {} end
        table.insert(section.see, content)
    elseif tag == "usage" then
        if section.usage then
            section.usage = section.usage .. "\n" .. content
        elseif content:len() > 0 then
            section.usage = content
        end
    elseif isCustom then
        if not section.custom then section.custom = {} end
        table.insert(section.custom, {tag = tag, content = content})
    else
        error("Unknown field tag: " .. tag)
    end
end

local function parseDoc(path, doc)
    print("reading", path)
    io.input(path)

    local currentSection = nil
    local lastTagType, lastTag = nil, nil

	local lineNumber = 0
    while true do
        local line = io.read("*line")
        if not line then break end
		lineNumber = lineNumber + 1
		local commentStart, commentEnd = line:find("^%s*%-%-%-?%s*")
		if commentStart then
			local sub = line:sub(commentEnd+1)
			local tagType, tag, content = sub:match("^(["..tagTypeClass.."])([a-zA-Z]*)%s?(.*)$")
			if tagType and tag and content then
				lastTagType, lastTag = tagType, tag
				if tag:len() == 0 then
					lastTag = nil
					tagType = nil
				end
			else
				if lastTag then
					tagType, tag = lastTagType, lastTag
				end
				local _, linestart = line:find("^%s*%-%-%-?%s?") -- leave leading whitespace in
				content = line:sub(linestart+1)
			end

			if tagType == tagTypes.builtin then
				if sectionTags[tag] then
					currentSection = addSection(doc, tag, content)
				else
					addField(lineNumber, currentSection, tag, content, true)
				end
			elseif tagType == tagTypes.customSection then
				currentSection = addSection(doc, tag, content)
			elseif tagType == tagTypes.customField then
				addField(lineNumber, currentSection, tag, content, true)
			end
		else
			lastTag = nil
		end
    end

    return doc
end

local function getLink(name)
    return name:lower():gsub("[^%w ]", ""):gsub(" ", "-")
end

local function emitParamMd(list)
    for _, elem in ipairs(list) do
        if elem.description then
            io.write(("- *%s*: %s\n"):format(elem.name, elem.description))
        else
            io.write(("- *%s*\n"):format(elem.name))
        end
    end
    io.write("\n")
end

local function formatFunctionSig(section)
	local params = {}
	if section.params then
		for _, param in ipairs(section.params) do
			table.insert(params, param.name)
		end
	end
	local anchorId = getLink(section.name)

	return ("%s(%s)<a id=%q></a>"):format(section.name, table.concat(params, ", "), anchorId)
end

local function emitMarkdown(doc, path)
    io.output(path)
    print("writing", path)

	-- TOC
	io.write("# Modules/Classes \n\n")
    for _, section in ipairs(doc) do
        if section.tag == "module" then
			local name = section.name
			io.write(("* \\[module\\] [%s](#%s)\n"):format(name, getLink(name)))
        elseif section.tag == "class" then
			local name = section.name
			io.write(("* \\[class\\] [%s](#%s)\n"):format(name, getLink(name)))
        end
    end

	-- body
    for _, section in ipairs(doc) do
        local heading = ""
        for i = 1, #section.namePath do
            heading = heading .. "#"
        end
		if section.tag == "function" then
			io.write(heading .. " " .. formatFunctionSig(section) .. "\n\n")
		else
			io.write(heading .. " " .. section.name .. "\n\n")
		end

        if section.description then
            io.write(section.description .. "\n\n")
		else
			io.write(("*[%s]*\n\n"):format(section.tag))
        end

        if section.tag == "module" then
            -- do nothing special
        elseif section.tag == "class" then
            if section.params and #section.params > 0 then
                io.write("**Constructor Arguments**:\n")
                emitParamMd(section.params)
            end
            if section.fields and #section.fields > 0 then
                io.write("**Member variables**:\n")
                emitParamMd(section.fields)
            end
        elseif section.tag == "function" then
            if section.params and #section.params > 0 then
                io.write("**Parameters**:\n")
                emitParamMd(section.params)
            end
            if section.returnValue then
                io.write("**Return Value**: " .. section.returnValue .. "\n\n")
            end
        elseif section.tag == "table" then
            if section.fields and #section.fields > 0 then
                io.write("**Fields**:\n")
                emitParamMd(section.fields)
            end
        end

        if section.usage then
            io.write("**Usage**:\n```lua\n" .. section.usage .. "\n```\n\n")
        end

        if section.see then
            local links = {}
            for _, other in ipairs(section.see) do
                table.insert(links, ("[%s](#%s)"):format(other, getLink(other)))
            end
            io.write("**See also**: " .. table.concat(links, ", ") .. "\n")
        end
    end
end

local success, inspect = pcall(require, "inspect")
inspect = success and inspect

local argn = #arg

local doc = {}
for i=1, argn-1 do
	parseDoc(arg[i], doc)
end

if inspect then
	print(inspect(doc))
end
emitMarkdown(doc, arg[argn])

