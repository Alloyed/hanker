local Hanker = require 'hanker'

local test = {}

local english_lines = [[
1234567890
qwertyuiop
asdfghjkl.
zxcvbnm,?!
]]

local symbol_lines = [[
1234567890
!@#$%^&*()
-_=+[]{}\|
;:'"`~<>/?
]]

local keyboard = {}
keyboard.cursor = Hanker.newFixedGridCursor()
local CELL = 40

local prompt = Hanker.newLabel("Your name?", 32)
prompt:setAlign('center')
prompt:centerX():set(Hanker.CENTERX, 0)
prompt:top():set(Hanker.TOP, 100)
prompt:setWidth(CELL * 10)
prompt:setHeightLines(1)

local label = Hanker.newEditBox(32)
label:setAlign('center')
label:centerX():set(prompt:centerX(), 0)
label:top():set(prompt:bottom(), 10)
label:setWidth(CELL * 10)
label:setHeightLines(1)
label:setCharacterLimit(30)
label:setPlaceholderText("Link")
--label:setPasswordMode(true)

local function cell(control, w, h)
	w = w or 1
	h = h or 1
	control:setTextAlign('center', 'center')
	control:setWidth((w*CELL) - 2)
	control:setHeight((h*CELL) - 2)
end

keyboard.page = {}
do
	local y = 0
	for line in english_lines:gmatch("[^\n]+") do
		y = y + 1
		local x = 0
		for char in line:gmatch(".") do
			x = x + 1
			local key = Hanker.newButton(char, 24)
			key:left():set(label:left(), (x-1) * CELL)
			key:top():set(label:bottom(), (y-1) * CELL + 10)
			cell(key)
			key._textinput = char
			keyboard.cursor:setGridControl(key, x, y)
			table.insert(keyboard.page, key)
		end
	end
end

local function swap_keyboards(lines)
	local i = 0
	for line in lines:gmatch("[^\n]+") do
		for char in line:gmatch(".") do
			i = i + 1
			local key = keyboard.page[i]
			key:setText(char)
			key._textinput = char
		end
	end
end

local y = 5
local capslock = Hanker.newButton("Aa", 16)
capslock:left():set(label:left(), 0)
capslock:top():set(label:bottom(), (y-1)*CELL+10)
cell(capslock)
keyboard.cursor:setGridControl(capslock, 1, y)

local symbolslock = Hanker.newButton("?123", 16)
symbolslock:left():set(capslock:right(), 2)
symbolslock:top():set(capslock:top(), 0)
cell(symbolslock)
keyboard.cursor:setGridControl(symbolslock, 2, y)

local spacekey = Hanker.newButton("", 24)
spacekey:left():set(symbolslock:right(), 2)
spacekey:top():set(symbolslock:top(), 0)
cell(spacekey, 4, 1)
keyboard.cursor:setGridControl(spacekey, 3, y)
keyboard.cursor:setGridControl(spacekey, 4, y)
keyboard.cursor:setGridControl(spacekey, 5, y)
keyboard.cursor:setGridControl(spacekey, 6, y)

local delkey = Hanker.newButton("delete", 16)
delkey:left():set(spacekey:right(), 2)
delkey:top():set(spacekey:top(), 0)
cell(delkey, 2, 1)
keyboard.cursor:setGridControl(delkey, 7, y)
keyboard.cursor:setGridControl(delkey, 8, y)

local submitkey = Hanker.newButton("enter", 16)
submitkey:left():set(delkey:right(), 2)
submitkey:top():set(delkey:top(), 0)
cell(submitkey, 2, 1)
keyboard.cursor:setGridControl(submitkey, 9, y)
keyboard.cursor:setGridControl(submitkey, 10, y)

local shouldTitleCase = true
local caps = true
local symbols = false
local function refresh_keyboard()
	if symbols then
		swap_keyboards(symbol_lines)
	elseif caps then
		swap_keyboards(string.upper(english_lines))
	else
		swap_keyboards(string.lower(english_lines))
	end
end

refresh_keyboard()
function test.update()
	keyboard.cursor()

	for _, key in ipairs(keyboard.page) do
		if key():isClicked() then
			label:insertTextAtCursor(key._textinput)
			if caps then
				caps = false
				refresh_keyboard()
			end
		end
	end

	if capslock():isClicked() then
		if symbols then
			symbols = not symbols
		else
			caps = not caps
		end
		refresh_keyboard()
	end

	if symbolslock():isClicked() then
		symbols = not symbols
		refresh_keyboard()
	end

	if spacekey():isClicked() then
		label:insertTextAtCursor(" ")
		if not caps and shouldTitleCase then
			caps = true
			refresh_keyboard()
		end
	end

	if delkey():isClicked() then
		label:deleteText('character')
	end

	if submitkey():isClicked() then
		label:setEditText("")
	end

	prompt()
	label()
end

return test
