-- revolver thing
local Hanker = require 'hanker'
local Anchor = Hanker.Anchor

local buttons = { }

local cursor = Hanker.newFixedGridCursor({shouldWrapX = true})

for i=0, 7 do
	local btn = Hanker.newButton(("button%d"):format(i), 14)
	btn:setTextAlign('center', 'center')
	btn:setAlign('center', 'center')
	btn:setWidth(60)
	btn:setHeight(60)
	cursor:addColumn(btn)
	table.insert(buttons, btn)
end

local infoText = "This is the element you have currently selected"
local clickedInfoText = "WOAH YOU CLICKED ME"
local info = Hanker.newLabel(text, 12)
info:setTextAlign('center', 'center')
info:setAlign('center', 'bottom')
info:setWidth(60)
info:useFloatingHeight()

local TWOPI = math.pi * 2
local function idk(t, length)
	return t - math.floor(t/length) * length
end
local function lerpAngle(a, b, t)
	local num = idk(b-a, TWOPI)
	if num > math.pi then
		num = num - TWOPI
	end
	return a + num * t
end

local test = {}

local rot
local targetRot = 0
local lastSelectedControl
function test.update(dt)
	cursor()

	for i, btn in ipairs(buttons) do
		if cursor:getSelectedControl() == btn then
			targetRot = -(math.pi*(i+2)/4)
			if rot == nil then rot = targetRot end
			if lastSelectedControl ~= btn then
				info:setText(infoText)
				lastSelectedControl = btn
			end
		end
	end

	-- ghetto quadout tween
	rot = lerpAngle(rot, targetRot, dt*10)

	for i, btn in ipairs(buttons) do
		local theta = (math.pi*i/4) + rot
		local dist = 100
		local dx, dy = math.cos(theta)*dist, math.sin(theta)*dist
		btn:centerX():setRelative(Anchor.CENTERX, dx)
		btn:centerY():setRelative(Anchor.CENTERY, dy)

		btn()

		if btn:isSelected() then
			info:centerX():setRelative(btn:centerX())
			info:bottom():setRelative(btn:top(), -10)
			info()
		end

		if btn:isClicked() then
			if btn:isSelected() then
				-- imagine this swapped weapons in your game and closed this
				-- menu or something
				info:setText(clickedInfoText)
				print(i)
			else
				cursor:selectControl(btn)
			end
		end
	end
end

return test
