local app = {
	globals = {
		"imgui",
		"PROF_CAPTURE",
		"PROF_REALTIME",
	}
}

stds.app = app
std   = "luajit+love+app"

ignore={"431", "212", "21/._*"}

exclude_files = {"rocks"}
