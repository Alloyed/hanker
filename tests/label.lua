local Hanker = require 'hanker'

local test = {}

local hello = Hanker.newLabel("Hey it's me,\nyour long-ass label\nI hope I work~", 20)
hello:setAlign('left', 'top')
hello:setTextAlign('center')
hello:setWidth(100)
hello:useFloatingHeight()
hello:top():setRelative(Hanker.TOP, 100)
hello:left():setRelative(Hanker.LEFT, 10)

local constrained = Hanker.newLabel("Since I'm constrained on both ends you don't need to define a width")
constrained:setTextAlign('center')
constrained:useFloatingHeight()
constrained:top():set(hello:bottom(), 10) -- set is an alias of setRelative
constrained:left():set(hello:left())
constrained:right():set(hello:right())

local leftAligned = Hanker.newLabel({
	text = "I'm on the left",
	drawFn = function(self, x, y, w, h)
		love.graphics.setColor(1, 1, 1, 1)
		love.graphics.rectangle('line', x, y, w, h)
		love.graphics.setColor(0, 1, 0, 1)
		self:drawText(x, y, w, h)
	end,
})
leftAligned:setTextAlign('left')
leftAligned:setWidth(50)
leftAligned:setHeight(50)
leftAligned:top():set(constrained:bottom(), 10)
leftAligned:left():set(constrained:left())

local rightAligned = Hanker.newLabel("Right side, but also cut off for being too dang long", 8)
rightAligned:setAlign('right')
rightAligned:setTextAlign('right')
rightAligned:top():set(leftAligned:top())
rightAligned:left():set(leftAligned:right())
rightAligned:bottom():set(leftAligned:bottom())
rightAligned:right():set(constrained:right())

local perfectCenter = Hanker.newLabel("Confirmed better than HTML", 10)
perfectCenter:setAlign('center', 'center')
perfectCenter:setTextAlign('center')
perfectCenter:setWidth(100)
perfectCenter:useFloatingHeight()
perfectCenter:centerX():set(Hanker.CENTERX)
perfectCenter:centerY():set(Hanker.CENTERY)

local clickMe = Hanker.newButton("Click me", 60)
clickMe:setAlign('center')
clickMe:setTextAlign('center')
clickMe:setWidth(300)
clickMe:setHeight(80)
clickMe:top():setRelative(perfectCenter:bottom(), 10)
clickMe:centerX():setRelative(perfectCenter:centerX())

local show = true
function test.update()
	if show then
		hello()
		constrained()
		leftAligned()
		rightAligned()
	end
	perfectCenter()

	if clickMe():isClicked() then
		show = not show
	end
end

return test
