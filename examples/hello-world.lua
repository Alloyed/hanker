local Hanker = require 'hanker'

-- app code
local myLabel = Hanker.newLabel("Hello World")
myLabel:setAlign('center', 'center')
myLabel:setTextAlign('center', 'center')
myLabel:centerX():set(Hanker.CENTERX)
myLabel:centerY():set(Hanker.CENTERY)
myLabel:setWidth(200)
-- end app code

function love.load(argv)
	-- Sets the "unscaled" dimensions. The UI will be pixel perfect at this
	-- resolution.
	Hanker.setUnscaledDimensions(800, 600)
	-- Sets the viewport. Internally all measurements are in unscaled "Anchor
	-- Units", but when it comes time to draw/handle mouse input they get
	-- converted to "Viewport units". (0, 0) in anchor units is the top-left of
	-- the viewport, and (UnscaledWidth, UnscaledHeight) is the bottom-right of
	-- the viewport. Usually (0, 0, getDimensions()) is a good default.
	Hanker.setViewport(0, 0, love.graphics.getDimensions())
	-- This is nice for text boxes.
	love.keyboard.setKeyRepeat(true)
end

function love.update(dt)
	-- Updates all objects that need to update. this is used for some built-in
	-- animations
	Hanker.updateFrame(dt)

	-- app code
	myLabel()
	-- end app code
end

function love.draw()
	-- Draw the UI.
	Hanker.draw()
end

function love.resize(w, h)
	-- Resize the UI to fit the new screen size
	Hanker.setViewport(0, 0, w, h)
end

--
-- User inputs
--
function love.textinput(t)
	-- Send text input event to UI. This is used for text boxes.
	Hanker.textInput(t)
end

function love.mousemoved(x, y)
	-- Send mouse moved event to UI. This is the main way we update the mouse
	-- inside the UI.
	Hanker.mouseMoved(x, y)
end

function love.mousepressed(x, y, button)
	-- Send mouse pressed event to UI. This is the main way we press buttons.
	Hanker.mousePressed(x, y, button)
end

function love.mousereleased(x, y, button)
	-- Send mouse released event to UI. Buttons need both a pressed event and a
	-- released event to work, so be sure to define both.
	Hanker.mouseReleased(x, y, button)
end

function love.wheelmoved(x, y)
	-- Send mouse wheel moved event to UI. This is used to scroll.
	Hanker.wheelMoved(x, y)
end

-- The stuff below here is for handling arrow keys, tab/shift tab etc. you
-- probably don't need this if you're just doing buttons that you control with
-- your mouse
local keymap = {
	x = "confirm",
	z = "cancel",
	c = "options",
	up = "up",
	down = "down",
	left = "left",
	right = "right",
	pageup = "pageup",
	pagedown = "pagedown",
	home = "first",
	["end"] = "last",
}

function love.keypressed(key)
	-- Handles non-text key presses for edit boxes. these are things like
	-- backspace, ctrl-left etc. if an edit boxes is selected this returns
	-- true, which means we don't want to pass the same event to other
	-- controls/subsystems
	if Hanker.keyboardKeyPressed(key) then
		return
	end

	-- Cursor handling code: this handles navigating between UI elements
	-- as you would for example with arrow keys on your keyboard. This is
	-- mapped to a keyboard here using the built-in keymap, but you could
	-- equally map it to a game controller or even something more exotic.
	local cursorKey = keymap[key]
	if cursorKey then
		Hanker.cursorKeyPressed(cursorKey)
	elseif key == 'tab' then
		if love.keyboard.isDown('lshift', 'rshift') then
			Hanker.cursorKeyPressed('previous')
		else
			Hanker.cursorKeyPressed('next')
		end
	end
end

function love.keyreleased(key)
	-- Cursor handling code: this handles navigating between UI elements
	-- as you would for example with arrow keys on your keyboard. This is
	-- mapped to a keyboard here using the built-in keymap, but you could
	-- equally map it to a game controller or even something more exotic.
	local cursorKey = keymap[key]
	if cursorKey then
		Hanker.cursorKeyReleased(cursorKey)
	elseif key == 'tab' then
		Hanker.cursorKeyReleased('next')
		Hanker.cursorKeyReleased('previous')
	end
end
