-- simple scrollList test
local util = require 'hanker.util'
local Hanker = require 'hanker'

local test = {}

local function lerp(a, b, t)
	return a + (b-a)*t
end
local list
do
	local function drawRow(self)
		local x, y, w, h = self.box:getAABB()
		self:applyMask()
		if self.selected or self.over then
			love.graphics.setColor(.3, .3, .5, self.alpha * (.3 + math.sin(love.timer.getTime()) * .05))
			love.graphics.rectangle('fill', x, y, w, h)
		end
		love.graphics.intersectScissor(x, y, w, h)
		if self.selected or self.over then
			love.graphics.setColor(1, 1, 1, self.alpha)
		else
			love.graphics.setColor(.7, .7, .7, self.alpha)
		end

		self:drawText(x, y, w, h)
		love.graphics.setScissor()
	end

	local function rowInit(entry, i)
		local btn = Hanker.newButton("")
		btn:setTextAlign('center', 'top')
		btn.draw = drawRow
		return btn
	end

	local buttonHeight = 18
	local function drawColumnHeader(self)
		local x, y, w, h = self.box:getAABB()
		self:applyMask()
		love.graphics.intersectScissor(x, y, w, h)
		if self.selected or self.over then
			love.graphics.setColor(1, 1, 1, 1)
		else
			love.graphics.setColor(.7, .7, .7, 1)
		end
		self:drawText(x, y, w, h)
		love.graphics.setScissor()
	end

	local function columnInit(entry, i)
		local btn = Hanker.newButton("", 20)
		btn.draw = drawColumnHeader
		btn:setTextAlign('center', 'bottom')
		return btn
	end

	local entries = {}
	for i=1, 20 do
		local entry = {}

		local column = Hanker.newScrollList(buttonHeight, rowInit, 'y', entry)
		column:setControlPadding(2, 0)
		column:focus():setLerp(column:top(), column:bottom(), .1)

		local scrollBar = Hanker.newClickable()
		function scrollBar:draw()
			local x, y, w, h = self.box:getAABB()
			love.graphics.setScissor(x, y, w, h)
			self:applyMask()
			love.graphics.setColor(.5, .5, .5, 1)
			love.graphics.rectangle('fill', x, y, w, h)

			local list = self.list
			local totalSize = list:getTotalScrollSize()
			local yFrac = list:getScrollOffset() / totalSize
			local hFrac = list:getViewScrollSize() / totalSize

			local scrollY = util.lerp(y, y + h, yFrac)
			local scrollH = hFrac * h

			love.graphics.setColor(.9, .9, .9, 1)
			love.graphics.rectangle('fill', x, scrollY, w, scrollH)
			love.graphics.setScissor()
		end
		function scrollBar:updateListScroll()
			local my = self.context.mouseY
			local _, y, _, h = self.box:getAABB()
			local list = self.list
			local totalSize = list:getTotalScrollSize()

			local hFrac = list:getViewScrollSize() / totalSize
			local scrollH = hFrac * h

			local yFrac = (my - (scrollH*.5) - y) / h
			local offset = yFrac * totalSize

			list:scroll(offset, 'instant')
		end
		scrollBar:setWidth(4)
		scrollBar:top():setRelative(column:top())
		scrollBar:bottom():setRelative(column:bottom())
		scrollBar:left():setRelative(column:right(), 2)
		scrollBar.list = column

		local cursor = Hanker.newScrollListCursor(column, {})

		entry.list = column
		entry.scrollbar = scrollBar
		entry.cursor = cursor
		for j=1, 100 do
			local row = {name = ("AAA %d.%d"):format(i, j)}
			table.insert(entry.list:getEntries(), row)
		end

		table.insert(entries, entry)
	end

	local columnWidth = 200
	list = Hanker.newScrollList(columnWidth, columnInit, 'x', entries)
	list:setShouldClamp(false, false)
	list:setControlPadding(0, 4)
	list:setHeight(50)
	list:left():setRelative(Hanker.Anchor.LEFT)
	list:right():setRelative(Hanker.Anchor.RIGHT)
	list:top():setAbsolute(30)
	list:focus():setLerp(list:left(), list:right(), 1/3)
end

local tooltip = Hanker.newContainer({})
tooltip._.Header = Hanker.newLabel("", 20)
tooltip._.Header:setTextAlign('center')
tooltip._.Header:setWidth(200)
tooltip._.Header:useFloatingHeight()

tooltip._.Body = Hanker.newLabel("", 12)
tooltip._.Body:top():setRelative(tooltip._.Header:bottom(), 5)
tooltip._.Body:left():setRelative(tooltip._.Header:left())
tooltip._.Body:right():setRelative(tooltip._.Header:right())
tooltip._.Body:setTextAlign('center')
tooltip._.Body:useFloatingHeight()

local cursor = Hanker.newScrollListCursor(list, {
	useFixedScrollPoint = true,
})

function test.update(dt)
	cursor()
	list()

	if cursor:isLeaving() then
		love.event.push('quit')
	end

	for columnIdx, label in list:iterateActiveControls() do
		label:setText(string.format("Column %d", columnIdx))

		label.alpha = label.alpha or 0
		label.targetAlpha = label:isSelected() and 1 or 0
		label.alpha = lerp(label.alpha, label.targetAlpha, label:isSelected() and .05 or .1)

		label.floatTextSize = label.floatTextSize or label.textSize
		label.targetTextSize = label.selected and 40 or 20
		label.floatTextSize = lerp(label.floatTextSize, label.targetTextSize, .1)
		label:setTextSize(math.floor(label.floatTextSize))

		if label:isClicked() then
			cursor:selectIndex(columnIdx)
		end

		local col = label.entry.list
		col:top():setRelative(label:bottom(), 4)
		col:left():setRelative(label:left(), 40)
		col:right():setRelative(label:right(), -40)
		col:setHeight(300)
		col(label.alpha < .01 and 'hide')

		if label:isSelected() then
			-- FIXME: ordering issue, when we run the cursor we will calculate
			-- the initial offset by looking at the height of the column, which
			-- will be 0 until we set the height. I don't think there's a great
			-- answer for this one.
			label.entry.cursor()

			local scrollbar = label.entry.scrollbar()
			if scrollbar:isPressed() then
				scrollbar:updateListScroll()
			end

			local selectedEntry
			for rowIdx, row in col:iterateActiveControls() do
				row:setText(row.entry.name)

				-- alpha
				row.alpha = label.alpha

				if row.over then
					selectedEntry = row.entry
				end

				if row:isClicked() then
					label.entry.cursor:selectIndex(rowIdx)
				end
			end

			selectedEntry = selectedEntry or label.entry.cursor:getSelectedEntry()
			if selectedEntry then
				tooltip()
				tooltip._.Header:left():setRelative(col:right(), 10)
				tooltip._.Header:top():setRelative(col:focus())
				tooltip._.Header:setText("row")
				tooltip._.Body:setText(("you have selected the row that says %q on it. Great job, proud of you~"):format(selectedEntry.name))
			end
		else
			for _, row in col:iterateActiveControls() do
				row:setText(row.entry.name)

				-- scale
				row.scale = row.scale or 1
				row.targetScale = row.selected and 1.1 or 1
				row.scale = lerp(row.scale, row.targetScale, .1)

				-- alpha
				row.alpha = label.alpha
			end
		end

	end

end

return test
